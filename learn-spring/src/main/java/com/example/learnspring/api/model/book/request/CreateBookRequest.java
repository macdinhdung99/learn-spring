package com.example.learnspring.api.model.book.request;

import com.example.learnspring.api.model.annotation.NamePeople;
import com.example.learnspring.persistent.entity.Book;
import lombok.Data;
import org.springframework.beans.BeanUtils;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class CreateBookRequest {

    @NotBlank(message = "Name is mandatory")
    private String name;

    @NamePeople
    private String author;

    @NotBlank(message = "price is mandatory")
    private String price_gia;

    @NotNull(message = "Catalog_id is mandatory")
    private Long catalogId;
    // TODO xu ly validate


    public Book toBook() {
        Book book = new Book();
        BeanUtils.copyProperties(this,book);
        return book;
    }
}
