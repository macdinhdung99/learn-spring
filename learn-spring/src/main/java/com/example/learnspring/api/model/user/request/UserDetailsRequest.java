package com.example.learnspring.api.model.user.request;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Data
public class UserDetailsRequest {
    @NotNull
    private String name;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    @NotNull
    private boolean genderType;

    @NotNull
    private String password;

    @NotNull
    private String status;
}
