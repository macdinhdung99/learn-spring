package com.example.learnspring.api.model.custom_validate;

import com.example.learnspring.api.model.annotation.NamePeople;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NameValidate implements ConstraintValidator<NamePeople, String> {
    @Override
    public void initialize(NamePeople constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return s.length()> 10;
    }
}
