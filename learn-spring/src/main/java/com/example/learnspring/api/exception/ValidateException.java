package com.example.learnspring.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;
@RestControllerAdvice
public class ValidateException extends RuntimeException{
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String errorMessage = error.getDefaultMessage();
            String fieldName = ((FieldError) error).getField();
            switch (fieldName) {
                case "name" : {
                    errorMessage = "Sai ten tac pham";
                    break;
                }
                case "price": {
                    errorMessage = "Sai gia";
                    break;
                }
                case "catalogId": {
                    errorMessage = "Sai the loai";
                    break;
                }
            }

            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}
