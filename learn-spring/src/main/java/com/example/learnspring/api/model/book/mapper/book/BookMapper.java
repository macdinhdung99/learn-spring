package com.example.learnspring.api.model.book.request;

import com.example.learnspring.persistent.entity.Book;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface BookMapper {
    BookMapper INSTANCE = Mappers.getMapper( BookMapper.class );

    @Mapping(source = "name", target = "name")
    @Mapping(source = "author", target = "author")
    @Mapping(source = "price_gia", target = "price")
    @Mapping(source = "catalogId", target = "catalogId")
    Book bookRequestToBook(CreateBookRequest bookRequest);
}
