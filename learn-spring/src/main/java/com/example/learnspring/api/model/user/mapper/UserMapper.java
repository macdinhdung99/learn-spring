package com.example.learnspring.api.model.user.mapper;

import com.example.learnspring.api.model.user.request.UserDetailsRequest;
import com.example.learnspring.persistent.entity.UserDetails;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {
    UserMapper INSTANCE = Mappers.getMapper( UserMapper.class );

    @Mapping(source = "genderType", target = "gender", qualifiedByName ="genderBtoGenderString")
    UserDetails requestUserDetailsToUserDetails(UserDetailsRequest userDetailsRequest);

    @Named("genderBtoGenderString")
    public static String inchToCentimeter(boolean genderType) {
        String type = "Male";
        if(genderType) {
            type = "Female";
        }
        return type;
    }

    @Mapping(source = "genderType", target = "gender", qualifiedByName ="genderBtoGenderString")
    UserDetails requestUserDetailsToUserDetails2(UserDetailsRequest userDetailsRequest);

}
