package com.example.learnspring.api.model.book.response;

import com.example.learnspring.persistent.entity.Book;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.util.List;

@Data
public class ListBookResponse {
    private String name;

    private String author;

    private String price;

    private Long catalogId;

}
