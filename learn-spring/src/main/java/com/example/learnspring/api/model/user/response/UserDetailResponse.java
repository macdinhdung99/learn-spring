package com.example.learnspring.api.model.user.response;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UserDetailResponse {
    @NotNull
    private String name;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    @NotNull
    private boolean genderType;

    @NotNull
    private String password;

    @NotNull
    private String status;
}
