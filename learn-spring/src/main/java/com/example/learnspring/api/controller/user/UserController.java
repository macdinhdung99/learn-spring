package com.example.learnspring.api.controller.user;

import com.example.learnspring.api.model.user.mapper.UserMapper;
import com.example.learnspring.api.model.user.request.UserDetailsRequest;
import com.example.learnspring.persistent.entity.UserDetails;
import com.example.learnspring.service.IU.IUUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {
    //tim hieu Autowired. cai nào recommend
    @Autowired
    IUUserDetails iuUserDetails;

    // bo sung them phan validation.
    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<String> createBook(@RequestBody @Valid UserDetailsRequest request){
        iuUserDetails.createUserDetail(UserMapper.INSTANCE.requestUserDetailsToUserDetails(request));
        return new ResponseEntity<>("Tạo mới thành công",null,HttpStatus.CREATED);
    }
    @GetMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public List<UserDetails> createBook(){
        List<UserDetails> list = iuUserDetails.getListUserDetail();
//        return new ResponseEntity<>("data: " + list,null,HttpStatus.OK);
        return list;
    }

    @GetMapping("getPage")
    @ResponseStatus(HttpStatus.CREATED)
    public Page<UserDetails> getPage(){
        Page<UserDetails> page = iuUserDetails.getPage();
        return page;
    }

    @GetMapping("getList")
    @ResponseStatus(HttpStatus.CREATED)
    public List<UserDetails> getList(){
        List<UserDetails> page = iuUserDetails.getPageByList();
        return page;
    }

    @GetMapping("getSlice")
    @ResponseStatus(HttpStatus.CREATED)
    public Slice<UserDetails> getSlice(){
        Slice<UserDetails> slice = iuUserDetails.getPageBySlice();
        return slice;
    }
    @GetMapping("searchByName")
    public Page<UserDetails> searchByName(@RequestParam(name = "name") String userName, @PageableDefault Pageable pageable){
        Page<UserDetails> page = iuUserDetails.searchByName(userName,pageable);
        return page;
    }

}
