package com.example.learnspring.api.controller;

import com.example.learnspring.api.model.book.request.CreateBookRequest;
import com.example.learnspring.persistent.entity.Book;
import com.example.learnspring.service.IU.IUBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/books")
public class BookController {

    //tim hieu Autowired. cai nào recommend
    @Autowired
    IUBookService iuBookService;


    // RestAPI (cach dat ten api)
    @GetMapping("")
    public List<Book> getListBook(){
        return  iuBookService.getListBook();
    }

    // bo sung them phan validation.
    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<String> createBook(@RequestBody @Valid CreateBookRequest request){
        iuBookService.createBook(request);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Custom-Header", "foo");
        return new ResponseEntity<>("Successfull2", headers ,HttpStatus.CREATED);
    }

//    @ResponseStatus(HttpStatus.BAD_REQUEST)
//    @ExceptionHandler(MethodArgumentNotValidException.class)
//    public Map<String, String> handleValidationExceptions(
//            MethodArgumentNotValidException ex) {
//        Map<String, String> errors = new HashMap<>();
//        ex.getBindingResult().getAllErrors().forEach((error) -> {
//            String errorMessage = error.getDefaultMessage();
//            String fieldName = ((FieldError) error).getField();
//            switch (fieldName) {
//                case "name" : {
//                    errorMessage = "Sai ten tac pham";
//                    break;
//                }
//                case "price": {
//                    errorMessage = "Sai gia";
//                    break;
//                }
//                case "catalogId": {
//                    errorMessage = "Sai the loai";
//                    break;
//                }
//            }
//
//            errors.put(fieldName, errorMessage);
//        });
//        return errors;
//    }

}
