package com.example.learnspring.api.model.annotation;
import com.example.learnspring.api.model.custom_validate.NameValidate;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = NameValidate.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface NamePeople {
    String message() default "Invalid name people";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
