package com.example.learnspring.persistent.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String author;
    private String price;
    @Column(name = "catalog_id")
    private Long catalogId;

}
