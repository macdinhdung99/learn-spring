package com.example.learnspring.persistent.repository;

import com.example.learnspring.persistent.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book, Long> {
}
