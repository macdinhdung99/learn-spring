package com.example.learnspring.persistent.repository;

import com.example.learnspring.persistent.entity.Catalog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CatalogRepository extends JpaRepository<Catalog,Long> {
}
