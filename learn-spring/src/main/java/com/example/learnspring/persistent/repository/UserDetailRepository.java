package com.example.learnspring.persistent.repository;

import com.example.learnspring.persistent.entity.UserDetails;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;

import javax.persistence.QueryHint;
import java.util.stream.Stream;

import static org.hibernate.jpa.QueryHints.HINT_FETCH_SIZE;

public interface UserDetailRepository extends JpaRepository<UserDetails, Long> {
    @QueryHints(value = @QueryHint(name = HINT_FETCH_SIZE, value = "" + Integer.MIN_VALUE))
    @Query(value = "select u from UserDetails u")
    public Stream<UserDetails> streamAll();

    @Query(value = "select u from UserDetails u where u.firstName like %:name%")
    public Page<UserDetails> findByName(@Param("name") String likeName, Pageable pageable);
}
