package com.example.learnspring.service.book_service;

import com.example.learnspring.persistent.entity.UserDetails;
import com.example.learnspring.persistent.repository.UserDetailRepository;
import com.example.learnspring.service.IU.IUUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class UserDetailService implements IUUserDetails {
    Pageable firstPageWithTwoElements = PageRequest.of(1, 2);
    @Autowired
    UserDetailRepository userDetailRepository;

    @Override
    @Transactional(readOnly = true)
    public List<UserDetails> getListUserDetail() {
//        Stream<UserDetails> userDetailsStream = userDetailRepository.streamAll();
//        List<UserDetails> list = userDetailsStream.map(item -> {
//            UserDetails obj = item;
//            obj.setName(item.getName().toUpperCase());
//            return obj;
//        }).collect(Collectors.toList());
        List<UserDetails> list = userDetailRepository.findAll();
        UserDetails u = userDetailRepository.findById((long) 1).get();
        return list;
    }

    @Override
    public Page getPage() {
        Page<UserDetails> page = userDetailRepository.findAll(firstPageWithTwoElements);
        return page;
    }

    @Override
    public List<UserDetails> getPageByList() {
        List<UserDetails> page = userDetailRepository.findAll(firstPageWithTwoElements).getContent();
        return page;
    }

    @Override
    public Slice<UserDetails> getPageBySlice() {
        Slice<UserDetails> page = userDetailRepository.findAll(firstPageWithTwoElements);
        page.isLast();
        return page;
    }

    @Override
    public Page<UserDetails> searchByName(String likeName, Pageable pageable) {
        Page<UserDetails> page = userDetailRepository.findByName(likeName,firstPageWithTwoElements);
        return page;
    }

    @Override
    public UserDetails createUserDetail(UserDetails userDetails) {
        return userDetailRepository.save(userDetails);
    }
}
