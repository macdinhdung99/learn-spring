package com.example.learnspring.service.IU;

import com.example.learnspring.persistent.entity.Catalog;

import java.util.List;

public interface IUCatalogService {
    List<Catalog> getList();
    Catalog createCatalog(Catalog catalog);
}
