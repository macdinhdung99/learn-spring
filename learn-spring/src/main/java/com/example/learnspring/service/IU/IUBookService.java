package com.example.learnspring.service.IU;

import com.example.learnspring.api.model.book.request.CreateBookRequest;
import com.example.learnspring.persistent.entity.Book;

import java.util.List;

public interface IUBookService {
    void createBook(CreateBookRequest book);
    List<Book> getListBook();
}
