package com.example.learnspring.service.book_service;

import com.example.learnspring.api.model.book.request.BookMapper;
import com.example.learnspring.api.model.book.request.CreateBookRequest;
import com.example.learnspring.persistent.entity.Book;
import com.example.learnspring.persistent.repository.BookRepository;
import com.example.learnspring.service.IU.IUBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class BookService implements IUBookService {
    @Autowired
    private BookRepository bookRepository;

    @Override
    public void createBook(CreateBookRequest request) {
        Book getBook = BookMapper.INSTANCE.bookRequestToBook(request);
       bookRepository.save(request.toBook());
    }

    @Override
    public List<Book> getListBook() {
        return bookRepository.findAll();
    }
}
