package com.example.learnspring.service.book_service;

import com.example.learnspring.persistent.entity.Catalog;
import com.example.learnspring.persistent.repository.CatalogRepository;
import com.example.learnspring.service.IU.IUCatalogService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class CatalogService implements IUCatalogService {
    @Autowired
    CatalogRepository catalogRepository;

    @Override
    public List<Catalog> getList() {
        return catalogRepository.findAll();
    }

    @Override
    public Catalog createCatalog(Catalog catalog) {
        return catalogRepository.save(catalog);
    }
}
