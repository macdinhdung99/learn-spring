package com.example.learnspring.service.IU;

import com.example.learnspring.persistent.entity.UserDetails;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;

import java.util.List;

public interface IUUserDetails {
    List<UserDetails> getListUserDetail();

    UserDetails createUserDetail(UserDetails userDetails);

    Page getPage();

    List<UserDetails> getPageByList();

    Slice<UserDetails> getPageBySlice();

    Page<UserDetails> searchByName(String likeName, Pageable pageable);
}
